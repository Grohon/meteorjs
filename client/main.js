import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

gVar=new ReactiveVar(666);

// Template.hello.onCreated(function helloOnCreated() {
//   // counter starts at 0
//   this.counter = new ReactiveVar(0);
// });
// Template.foysal.onCreated(function foysalOnCreated() {
//   // counter starts at 0
//   this.state = new ReactiveVar("some random text");
// });
// Template.hello.helpers({
//   counter() {
//     return Template.instance().counter.get();
//   },
// });

// Template.hello.events({
//   'click button'(event, instance) {
//     // increment the counter when button is clicked
//     instance.counter.set(instance.counter.get() + 1);
//   },
// });
// Template.foysal.helpers({
//   name:function(){
//     return 'Foysal';
//   },
//   age:function(){
//     return '23';
//   },
//   state(){
//     return Template.instance().state.get();
//   }
// });
// Template.foysal.events({
//   'click button'(event, instance){
//     // alert('huhaha!');
//     instance.state.set('my name is bond... abdul kuddus bond...');
//   },
// });
// Template.registerHelper( 'gFunc', ( string ) => {
//   return string.concat(this.gVar.get());
// });
// Template.form.events({
//   'submit #playerForm':function (event, template) {
//     event.preventDefault();
//     console.log('printed');
//     var player=$('#playerName').val();
//     console.log(player);
//     // alert(player);
//     /* global Players */
//     Players.insert({
//       name: player
//     });//Players.find({}).fetch() <- at console
//   }
// });
Template.twitter.events({
  'submit #twitterForm':function (event, template) {
    // event.preventDefault();
    var tweets=$('#tweet').val();
    $('#tweet').val("");
    console.log(tweets);
    
    /* global Twitter */
    Twitter.insert({
      tweets: tweets,
      date: new Date()
    });//Twitter.find({}).fetch() <- at console
    return false;
  }
});
Template.print_tweets.helpers({
  'print_tweets': function(){
    return Twitter.find({}, {sort: {date: -1}});
  }
});
Template.print_tweets.events({
  'click #delete': function(){
    Twitter.remove(this._id);
  }
});
Template.googleForm.events({
  'click .rElem': function(){
    GForm.remove({_id:this._id});
  },
  'submit #google-form': function(event, template){
    event.preventDefault();
    var name = $('#name').val();
    var gid = $('#gid').val();
    var email = $('#email').val();
    var city = $('#city').val();
    GForm.insert({
      name: name,
      gid: gid,
      email: email,
      city: city
    });
  }
});
Template.googleForm.helpers({
  list: function(){
    return GForm.find({});
  }
});